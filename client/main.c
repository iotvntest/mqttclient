/**
 * @author Duc Anh TA
 *
 * @date 2020/08
 *
 * @brief Using Eclipse Paho MQTT C client library
 * to connect to the IoT server's MQTT broker :
 *  - publishing messages to the server, 
 *  - subscribing to topics
 *  - receiving published messages from the server.
 *
 * @link https://github.com/eclipse/paho.mqtt.c
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>

#include "MQTTClient.h"


// Change following configuration according to your account and your usage

#define GATEWAY         "LivingRoom"        // Gateway or Node that your device are

// #define WRITE_API_KEY   "a6afa678ea7c4ab5337a8d0b231ac7a2"
// #define READ_API_KEY    "50676b43c881dcfef1ca043b36e85aa1"

#define WRITE_API_KEY   "272482d90580271ab60ada7724ee1a9f"
#define READ_API_KEY    "5da944b2bd26"

//====================

#define TOPIC_PUB       "iot/"WRITE_API_KEY"/"GATEWAY

#define TOPIC_SUB       "#"


// IoT Server's MQTT Broker

#define ADDRESS         "tcp://io2data.com:1883"

#define CLIENT_ID       READ_API_KEY"_"GATEWAY

#define USERNAME        "iot_user"

#define PASSWORD        "iot_user"

#define QOS             1

#define TIMEOUT         10000L


volatile MQTTClient_deliveryToken deliveredtoken;


void delivered(void *context, MQTTClient_deliveryToken dt)
{   
    printf("Message with token value %d delivery confirmed\n", dt);

    deliveredtoken = dt;
}


void connlost(void *context, char *cause)
{    
    printf("\nConnection lost\n");

    printf("     cause: %s\n", cause);
}


int msgarrvd(void *context,
             char *topicName,
             int  topicLen,
             MQTTClient_message *message)
{
    int i;

    char* payloadptr;


    printf("\n\nMessage arrived\n");

    printf("     topic: %s\n", topicName);

    printf("   message: ");


    payloadptr = (char*) message->payload;

    for (i = 0; i < message->payloadlen; i++)
    {
        putchar(*payloadptr++);
    }

    putchar('\n');


    MQTTClient_freeMessage(&message);

    MQTTClient_free(topicName);

    return 1;
}


int main(int argc, char* argv[])
{
    MQTTClient client;

    MQTTClient_connectOptions   conn_opts   = MQTTClient_connectOptions_initializer;

    MQTTClient_message          pubmsg      = MQTTClient_message_initializer;

    MQTTClient_deliveryToken    token;


    int rc;


    MQTTClient_create(&client,
                      ADDRESS,
                      CLIENT_ID,
                      MQTTCLIENT_PERSISTENCE_NONE,
                      NULL);


    conn_opts.username = USERNAME;

    conn_opts.password = PASSWORD;

    conn_opts.keepAliveInterval = 20;

    conn_opts.cleansession = 1;


    MQTTClient_setCallbacks(client,
                            NULL,
                            connlost,
                            msgarrvd,
                            delivered);


    if ((rc = MQTTClient_connect(client, &conn_opts)) != MQTTCLIENT_SUCCESS)
    {
        printf("Failed to connect, return code %d\n", rc);

        exit(EXIT_FAILURE);
    }


    printf("\nConnect to %s", ADDRESS);

    MQTTClient_subscribe(client, TOPIC_SUB, QOS);


    pubmsg.qos      = QOS;

    pubmsg.retained = 0;

    deliveredtoken  = 0;


    // Test publication

    unsigned char end = 0;

    float sensor_value = 1.0;

    unsigned char light = 0;


    char payload_buffer[ 256 ];

#define PAYLOAD_SEND_PATTERN    "{\"time\":%d,\"%s\":%f,\"%s\":%d}"

#define TEMP_NAME           "Temperature"

#define LIGHT_NAME          "Light"


    while(!end)
    {
        unsigned int timestamp = time(NULL);    // seconds;

        sensor_value += 0.01;

        light = (light == 0) ? 1 : 0;


        (void) sprintf( payload_buffer,
                        PAYLOAD_SEND_PATTERN,
                        timestamp,
                        TEMP_NAME,
                        sensor_value,
                        LIGHT_NAME,
                        light );


        printf("\n\nPayload : %s\n\n", payload_buffer);


        pubmsg.payload      = (void *) payload_buffer;

        pubmsg.payloadlen   = strlen(pubmsg.payload);

        MQTTClient_publishMessage(client, TOPIC_PUB, &pubmsg, &token);


        while(deliveredtoken != token);

        printf("message delivered, timestamp at %d\n", timestamp);

        sleep(5);
    }


    MQTTClient_disconnect(client, 10000);

    MQTTClient_destroy(&client);

    return rc;
}
